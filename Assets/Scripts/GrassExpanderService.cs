﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;



public class GrassExpanderService : MonoBehaviour {

	public string expanderTag = "Grass";
	public float expanderRadius = 3;
	public float expanderLimitOnOneObject = 50;

	IList<GameObject> grassPrefabs;
	// Use this for initialization
	void Start () {
		grassPrefabs = GameObject.FindGameObjectsWithTag(expanderTag);
		foreach (GameObject grassObject in grassPrefabs) {
			CreateExpanderObject(grassObject);
			Vector3 randomRotation = new Vector3(0, Random.Range(0, 360), 0);
			grassObject.transform.rotation = Quaternion.Euler(randomRotation);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void CreateExpanderObject(GameObject expandedObject) {
		for (int i = 0; i < expanderLimitOnOneObject; i++) {
			float xRandom = Random.Range(expandedObject.transform.position.x - expanderRadius, expandedObject.transform.position.x + expanderRadius);
			float zRandom = Random.Range(expandedObject.transform.position.z - expanderRadius, expandedObject.transform.position.z + expanderRadius);
			Vector3 randomRotation = new Vector3(0, Random.Range(0, 360), 0);
			Vector3 newlyExpandedObjectPosition = new Vector3(xRandom, expandedObject.transform.position.y, zRandom);
			//Check if there is something beneath to be put on
			RaycastHit raycastHit;
			if (Physics.Raycast(newlyExpandedObjectPosition, Vector3.down, out raycastHit)) {
				if (raycastHit.collider != null) {
					Instantiate(expandedObject, raycastHit.point, Quaternion.Euler(randomRotation));
				}
			}
		}
	}
}
