﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	CharacterController characterController;
	GameManager gameManager;
	float speed;
	Quaternion orginalRotation;
	
	float maxHighPeek = 2f;
	bool isJumping = false;
	bool grounded = false;
	public float groundDist = 0.5f;
	float transformYPosition;
	Vector3 movement;
	Animator animator;

	// Use this for initialization
	void Start () {
		animator = GetComponentInChildren<Animator>();
		gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
		characterController = GetComponent<CharacterController>();
		orginalRotation = transform.rotation;
		
	}
	
	// Update is called once per frame
	void Update () {
		if (gameManager.gameStarted) {
			CheckGroundDistance();
			bool buttonOrTouchOccured = (Input.GetButtonDown("Fire1") || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began));
			

			if (buttonOrTouchOccured && grounded) {
				isJumping = true;
				transformYPosition = transform.position.y;
			}
			CheckJumpPeek();
		}
	}

	void FixedUpdate() {
		if (gameManager.gameStarted) {
			Move();
		}
	}

	void CheckGroundDistance() {
		RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit)){
            if (hit.distance < groundDist) {
				animator.SetBool("Jump", false);
				grounded = true;
			} else {
				animator.SetBool("Jump", true);
				grounded = false;
			}
		}
	}

	void Move() {
		speed = gameManager.speed;//!grounded ? gameManager.basicSpeed : gameManager.speed;
		movement = transform.forward;
		movement *= speed;

		if (isJumping) {
			movement.y += gameManager.jumpSpeed * Time.deltaTime;
		}
		if (!grounded && !isJumping) {
			movement.y -= gameManager.gravity * Time.deltaTime;
		}
		characterController.Move(movement * Time.deltaTime);
	}

	void CheckJumpPeek() {
		if (transform.position.y - transformYPosition >= maxHighPeek) {
			isJumping = false;
		}
	}

	private void OnTriggerEnter(Collider other) {
		if(other.CompareTag("Goal")) {
			gameManager.Score();
		}
		if(other.CompareTag("EndPlace")) {
			transform.position = GameObject.FindGameObjectWithTag("StartPlace").transform.position;
			ReturnRotationToOrginalRotation();
		}
		if(other.CompareTag("Killer")) {
			Debug.Log("Dead");
			gameManager.Dead();
		}
	}

	private void ReturnRotationToOrginalRotation() {
		transform.rotation = orginalRotation;
	}

	public void ReturnPositionAndRotationToOriginal() {
		transform.position = GameObject.FindGameObjectWithTag("StartPlace").transform.position;
		ReturnRotationToOrginalRotation();
	}

}
