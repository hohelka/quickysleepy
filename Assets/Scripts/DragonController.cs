﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonController : MonoBehaviour {

	private GameManager gameManager;
	// Use this for initialization
	void Start () {
		gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Breath() {
		gameManager.gameStarted = true;
	}
}
