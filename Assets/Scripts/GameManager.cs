﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {


	public static GameManager instance = null;
	public float score = 0;
	public float basicSpeed = 2f;
	public float speed = 2f;
	public float basicJumpSpeed = 120f;
	public float basicGravity = 120f;
	public float jumpSpeed = 120f;
	public float gravity = 120f;
	public float percentChange = 10f;

	public bool gameStarted = false;
	public bool levelLoaded = false;
	public GameObject fire;
	
	Text scoreText;
	RectTransform tapPanel;

	Animator dragonAnimator;
	Rect menuButton;

	public GameObject menu;
	public GameObject game;

	public LevelManager levelManager;


	// Use this for initialization

	void Awake() {
		if (instance == null) {
            instance = this;
		} else if (instance != this) {
           Destroy(gameObject);  
		}  
        DontDestroyOnLoad(gameObject);
		levelManager = GetComponent<LevelManager>();
		FindComponents();
		menu.SetActive(true);
		game.SetActive(false);
	}
	void Start () {
		
	}

	void FindComponents() {
		menu = GameObject.Find("Menu");
		game = GameObject.Find("Game");
		fire = GameObject.FindGameObjectWithTag("Flame");
	}

	public void StartLevelThings() {
		fire.SetActive(false);
		dragonAnimator = GameObject.FindGameObjectWithTag("Dragon").GetComponent<Animator>();
		scoreText = GameObject.Find("HUD").transform.Find("ScoreText").GetComponent<Text>();
		tapPanel = GameObject.Find("HUD").transform.Find("TapPanel").GetComponent<RectTransform>();
		scoreText.text = "Score: " + score;
		InvokeRepeating("PlayAnimation", 0, 3);
		SetMenuButton();
	}

	public void SetMenuButton() {
		RectTransform menuButtonTransform = GameObject.FindGameObjectWithTag("Menu").GetComponent<RectTransform>();
		float dist = menuButtonTransform.rect.width;
		menuButton = new Rect(menuButtonTransform.position.x - dist, menuButtonTransform.position.y - dist, menuButtonTransform.position.x + dist, menuButtonTransform.position.y + dist);
	}
	
	// Update is called once per frame
	void Update () {
		if (levelLoaded) {
			if ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)) {
				if (menuButton.Contains(Input.GetTouch(0).position)) {
					Debug.Log("Back To menu");
					Dead();
					CancelInvoke();
					GetComponent<LevelManager>().BackToMenu();
					levelLoaded = false;
					return;
				}
			}
			if (Input.GetButton("Fire1") && !gameStarted) {
				tapPanel.gameObject.SetActive(false);
				CancelInvoke("PlayAnimation");
				dragonAnimator.SetTrigger("Start");
			}
			if (gameStarted) {
				fire.SetActive(true);
			}
		}
	}

	private PlayerController GetPlayer() {
		return GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
	}

	public void SetScene(float speed, float percentChange) {
		float oldSpeed = basicSpeed;
		this.speed = speed;
		this.basicSpeed = speed;
		this.percentChange = percentChange;
		float percentToChangeRest = this.speed / oldSpeed - 1;
		basicJumpSpeed = basicJumpSpeed + basicJumpSpeed * percentToChangeRest;
		basicGravity = basicGravity + basicGravity * percentToChangeRest;
		jumpSpeed = basicJumpSpeed;
		gravity = basicGravity;

	}
	void PlayAnimation() {
		tapPanel.GetComponent<Animator>().SetTrigger("PlayAnim");
	}

	public void Score() {
		score++;
		if (speed <= 20f) {
			increaseValues();
		}
	}

	void increaseValues() {

		scoreText.text = "Score: " + score;
		speed += speed * percentChange / 100f;
		gravity += gravity * percentChange / 100f; //enable if jump will not increase with speed
		jumpSpeed += jumpSpeed * percentChange / 100f;
	}

	public void Dead() {
		levelManager.SetPointsToCurrentLevel(score);
		levelManager.RefreshLevelPanels();
		score = 0;
		speed = basicSpeed;
		gravity = basicGravity;
		jumpSpeed = basicJumpSpeed;
		fire.SetActive(false);
		gameStarted = false;
		dragonAnimator.SetTrigger("End");
		tapPanel.gameObject.SetActive(true);
		scoreText.text = "Score: " + score;
		InvokeRepeating("PlayAnimation", 0, 3);
		GetPlayer().ReturnPositionAndRotationToOriginal();
	}
}
