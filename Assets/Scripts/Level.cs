﻿using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Level {

	public int levelNumber;
	public float pointsScored;
	public float pointsToPlay;
	public float basicSpeed;
	public float percentageChange;

	public Level(int levelNumber, float pointsToPlay, float basicSpeed, float percentageChange) {
		this.levelNumber = levelNumber;
		this.pointsScored = 0;
		this.pointsToPlay = pointsToPlay;
		this.basicSpeed = basicSpeed;
		this.percentageChange = percentageChange;
	}


}
