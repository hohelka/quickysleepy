﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

	private GameManager gameManager;
	public GameObject levelPanelInstance;

	public List<Level> levels;

	public GameObject levelContent;

	public int currentLevel;
	// Use this for initialization
	private void Start() {
		gameManager = GetComponent<GameManager>();
		levelContent = GameObject.FindGameObjectWithTag("LevelContent");
		RefreshLevelPanels();
	}

	public void RefreshLevelPanels() {
		RemoveChildrenInGameObject(levelContent);
		levels.ForEach(level => {
			GameObject levelPanel = Instantiate(levelPanelInstance);
			levelPanel.GetComponentInChildren<Button>().onClick.AddListener(delegate{ChangeLevel(level.levelNumber);});
			levelPanel.transform.Find("ScoreText").GetComponent<Text>().text = "Score: " + level.pointsScored;
			Level previousLevel = levels.Find(l => l.levelNumber.Equals(level.levelNumber-1));
			if (previousLevel != null && previousLevel.pointsScored < level.pointsToPlay) {
				levelPanel.GetComponentInChildren<Button>().enabled = false;
			}
			levelPanel.transform.Find("ToPlayText").GetComponent<Text>().text = "Score to play: " + level.pointsToPlay;
			levelPanel.transform.SetParent(levelContent.transform);
		});
	}

	private void RemoveChildrenInGameObject(GameObject gameObject) {
		List<GameObject> goToDelete = new List<GameObject>();
		for (int i = 0; i < gameObject.transform.childCount; i++) {
			for (int j = 0; j < gameObject.transform.GetChild(i).childCount; j++) {
				goToDelete.Add(gameObject.transform.GetChild(i).GetChild(j).gameObject);
			}
			goToDelete.Add(gameObject.transform.GetChild(i).gameObject);
		}
		goToDelete.ForEach(child => Destroy(child));
	}

	


	public void SetPointsToCurrentLevel(float score) {
		Level levelFound = levels.Find(l => l.levelNumber == currentLevel);
		if (levelFound.pointsScored < score) {
			levelFound.pointsScored = score;
		}
	}
	public void BackToMenu() {
		gameManager.menu.SetActive(true);
		gameManager.game.SetActive(false);
	}
	public void ChangeLevel(int levelNumber) {
		currentLevel = levelNumber;
		Level level = levels.Find((l) => l.levelNumber == levelNumber);
		gameManager.SetScene(level.basicSpeed, level.percentageChange);
		InitScene();
	}

	private void InitScene() {
		gameManager.menu.SetActive(false);
		gameManager.game.SetActive(true);
		gameManager.StartLevelThings();
		gameManager.levelLoaded = true;
	}
}
